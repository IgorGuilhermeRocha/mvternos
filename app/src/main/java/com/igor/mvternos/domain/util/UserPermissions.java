package com.igor.mvternos.domain.util;

import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * @autor Igor Guilherme Almeida Rocha
 * Classe reponsável por verificar e requisitar permissões ao usuário
 */
public class UserPermissions {

    public static Boolean verifyPermissions(Activity activity, final String[] PERMISSIONS, final Integer PERMISSIONS_CODE){

        List<String> permissionsDenied = new ArrayList<>();

        for(String permission : PERMISSIONS)
            if(ContextCompat.checkSelfPermission(activity.getApplicationContext(), permission)
                    != PackageManager.PERMISSION_GRANTED) permissionsDenied.add(permission);

        if(!permissionsDenied.isEmpty()){
            ActivityCompat.requestPermissions(activity, permissionsDenied.toArray(new String[permissionsDenied.size()]), PERMISSIONS_CODE);
            return false;
        } else return true;
    }

}
