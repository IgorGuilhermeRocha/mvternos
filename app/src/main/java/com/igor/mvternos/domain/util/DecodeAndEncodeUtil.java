package com.igor.mvternos.domain.util;

import android.util.Base64;


/**
 * @author Igor Guilherme Almeida Rocha
 * Classe que implementa os métodos para a codificação e  decodificação na base 64
 */
public class DecodeAndEncodeUtil {

    public static String encodeToBase64(String text){
        return Base64.encodeToString(text.getBytes(), Base64.DEFAULT);
    }

    public static String decodeBase64(String text){
        return new String(Base64.decode(text, Base64.DEFAULT));
    }


}
