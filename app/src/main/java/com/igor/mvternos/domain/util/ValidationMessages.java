package com.igor.mvternos.domain.util;

/**
 * @autor Igor Guilherme Almeida Rocha
 * Classe que tem como atributos as mensagens de validação dos formulários
 */
public class ValidationMessages {

    public static final String CPF_PATTERN_ERROR = "Cpf inválido";
    public static final String PHONE_PATTERN_ERROR = "Telefone inválido";
    public static final String EMAIL_ALREADY_USE_ERROR = "Este email, já esta em uso";
    public static final String EMAIL_BADLY_FORMATTED_ERROR = "Email inválido";
    public static final String PASSWORD_LENGTH_WARNING = "Senha muito curta";
    public static final String PASSWORD_MATCHES_WARNING = "Suas senhas estão diferentes";
    public static final String FIELDS_BLANK_WARNING = "Preencha todos os campos";

}
