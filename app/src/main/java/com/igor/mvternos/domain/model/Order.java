package com.igor.mvternos.domain.model;

import com.igor.mvternos.domain.model.enums.SuitSize;

public class Order {

    private String id;
    private Double price;
    private Suit suit;
    private Long quantity;
    private SuitSize size;

    public Order(){

    }

    public Order(Suit suit, Long quantity, SuitSize size, Double price){
        this.suit = suit;
        this.quantity = quantity;
        this.size = size;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public SuitSize getSize() {
        return size;
    }

    public void setSize(SuitSize size) {
        this.size = size;
    }
}
