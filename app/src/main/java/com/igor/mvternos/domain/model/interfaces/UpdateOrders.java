package com.igor.mvternos.domain.model.interfaces;

import com.igor.mvternos.domain.model.Order;

import java.util.List;

public interface UpdateOrders {

    void updateListOfOrders(List<Order> orders);
}
