package com.igor.mvternos.domain.model.interfaces;

import com.igor.mvternos.domain.model.Suit;

/**
 * @author Igor Guilherme Almeida Rocha
 * @see com.igor.mvternos.activity.MainActivity
 * @see com.igor.mvternos.ui.adjustments.AdjustmentsFragment
 * Interface que descreve o método que ira ser chamado quando um item da lista de ternos
 * é clicado
 */
public interface RecyclerSuitOnClick {

    void onItemClicked(Suit suit);
}
