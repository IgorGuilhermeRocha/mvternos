package com.igor.mvternos.domain.model.interfaces;

/**
 * @author Igor Guilherme Almeida Rocha
 * @see com.igor.mvternos.ui.suits.SuitsFragment
 * @see com.igor.mvternos.adapters.SuitAdapter
 * @see com.igor.mvternos.controller.SuitsFragmentController
 * Interface usada para implementarmos métodos de callback
 * Mais especificamente o fragment adjustments utiliza ela para
 * se comunicar com a MainActivity
 */
public interface SendMessageWhatsApp {

    void sendMessage(String text);
}
