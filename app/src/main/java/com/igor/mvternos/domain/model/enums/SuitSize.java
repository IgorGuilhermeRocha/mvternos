package com.igor.mvternos.domain.model.enums;

public enum SuitSize {

    P("P"),
    M("M"),
    G("G"),
    GG("GG"),
    XG("XG");

    private String size;

    private SuitSize(String size){
        this.size = size;
    }

    @Override
    public String toString() {
        return this.size;
    }



}
