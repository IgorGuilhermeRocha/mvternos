package com.igor.mvternos.domain.util;

import android.text.Editable;

/**
 * @author Igor Guilherme Almeida Rocha
 * Classe que valida as mascaras de cpf e telefone
 */
public class ValidationsMaskUtil {

    private static final Integer CPF_BREAK_POINT_1  = 4;
    private static final Integer CPF_BREAK_POINT_2 = 8;
    private static final Integer CPF_BREAK_POINT_3 = 12;
    private static final Integer CPF_SIZE = 15;

    private static final Integer PHONE_BREAK_POINT_1 = 1;
    private static final Integer PHONE_BREAK_POINT_2 = 4;
    private static final Integer PHONE_BREAK_POINT_3  = 7;
    private static final Integer PHONE_BREAK_POINT_4  = 12;


    private static final Integer PHONE_SIZE = 17;


    /**
     * valida o cpf
     * pattern -> 000.000.000-00
     * @param cpf Cpf digitado pelo usuário
     * @return true, se o cpf esta no padrão correto
     */
    public static Boolean validCpf(String cpf){
        return cpf.matches("([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})");
    }

    /**
     * Valida o telefone
     * pottern -> (00) 0-0000-00
     * @param phone Telefone digitado pelo usuáro
     * @return true, se o telefone esta no padrão correto
     */
    public static Boolean validPhone(String phone){
        return phone.matches("(\\([0-9]{2}\\)\\s[0-9]{1}-[0-9]{4}-[0-9]{4})");
    }

    /**
     * Valida senha
     * @param password Senha digitada pelo usuário
     * @return true, se a senha tem pelo menos 6 caracteres
     */
    public static Boolean validPassword(String password){
        return (password.length() >= 6) ? true : false;
    }


    /**
     * Método utilizado para ajudar o usuário na digitação do telefone
     * @param text Instancia de Editable, telefone atual
     * @param textLength Tamanho do telefone
     * @param lastDigit último digito
     */
    public static void verifyPhoneMask(Editable text, int textLength, String lastDigit){

        String textTemp =  text.toString();
        String charToApply = null;
        boolean isBreaked = false;
        boolean isNumber = lastDigit.matches("[0-9]");

        if(textLength >= PHONE_SIZE || (!lastDigit.matches("[0-9]"))) attEditableText(text, removeLastChar(textTemp));
        else{
            if(textLength == PHONE_BREAK_POINT_1 && isNumber){
                charToApply = "(";
                isBreaked = true;

            }else if(textLength == PHONE_BREAK_POINT_2 && isNumber){
                charToApply = ") ";
                isBreaked = true;

            }else if((textLength == PHONE_BREAK_POINT_3 || textLength == PHONE_BREAK_POINT_4) && isNumber){
                charToApply = "-";
                isBreaked = true;
            }

            if(isBreaked) attEditableText(text, applyBreak(textTemp, textLength,  charToApply, lastDigit));
        }

    }


    /**
     * Método utilizado para ajudar o usuário na digitação do cpf
     * @param text Instancia de Editable, Cpf atual
     * @param textLength tamanho do cpf
     * @param lastDigit último digito
     */
    public static void verifyCpfMask(Editable text, int textLength, String lastDigit) {
        String textTemp =  text.toString();

        if(textLength >= CPF_SIZE || (!lastDigit.matches("[0-9]"))) attEditableText(text, removeLastChar(textTemp));
        else {
            boolean isANumber = lastDigit.matches("[0-9]");
            String charToApply = null;

            if( ( textLength == CPF_BREAK_POINT_1 || textLength == CPF_BREAK_POINT_2) && isANumber) charToApply = ".";
            else if( textLength == CPF_BREAK_POINT_3 && isANumber) charToApply = "-";

            if(charToApply != null) attEditableText(text, applyBreak(textTemp, textLength, charToApply, lastDigit) );
        }
    }


    private static String applyBreak(String text, int position, String charToApply, String lastDigit){
         return  text.substring(0, position - 1) + charToApply + lastDigit;
    }

    /**
     * Remove último caractere do texto
     * @param text String que representa Cpf ou telefone
     * @return O texto sem o último caractere
     */
    private static String removeLastChar(String text){
        return text.substring(0, text.length() - 1);
    }

    /**
     * Atualiza o texto digitado pelo usuário
     * @param text Instancia de Editable, representa um cpf ou telefone
     * @param newtext Texto após a atualização
     */
    private static void attEditableText(Editable text, String newtext){
        text.clear();
        text.append(newtext);

    }



}
