package com.igor.mvternos.domain.util;


import android.content.Context;
import android.graphics.Bitmap;
import android.text.method.TransformationMethod;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.igor.mvternos.R;

import java.io.ByteArrayOutputStream;

/**
 * @author Igor Guilherme Almeida Rocha
 * Classe utiliária, alguns métodos que são utilizados em muitas activities / fragments
 * estão aqui
 */
public class Util {

    /**
     * @param fields Array de string com os valores dos campos
     * @return false se um dos campos estiver vazio, caso contrário true
     */
    public static Boolean validFields(String [] fields){
        for(String field : fields) if(field.trim().isEmpty()) return false;
        return true;
    }

    /**
     * Muda os fragments
     * @param fragment Um fragment
     * @param activity Activity do fragment
     * @param contentContainer id do container que ser substituido
     */
    public static void changeFragments(Fragment fragment, FragmentActivity activity, int contentContainer){
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(contentContainer, fragment);
        transaction.commit();
    }


    /**
     * Muda a visibilidade da senha em um editText
     * @param activity Uma instancia de FragmentActivity
     * @param btHideShow O botão responsável por disparar a ação
     * @param etPassword Campo da senha
     * @param transformation Um TransformationMethod, ele representa o novo estado do editText
     * @param imgId id da Imagem do botão
     * @param isHide boolean se true significa que a senha esta escondida
     * @return o estado atual do editText, se true significa que a senha esta escondida
     */
    public static Boolean transformEditText(FragmentActivity activity, Button btHideShow, EditText etPassword, TransformationMethod transformation, int imgId, boolean isHide){
        etPassword.setTransformationMethod(transformation);
        btHideShow.setBackground(activity.getDrawable(imgId));
        etPassword.setSelection(etPassword.getText().toString().length());
        isHide = !isHide;
        return isHide;
    }

    /**
     *  Transforma uma imagem em bitmap para um array de bytes
     * @param imgBitmap Imagem em bitmap
     * @param quality qualidade da imagem 0 - 100
     * @param format formato da imagem png, jpeg
     * @return um array de bytes
     */
    public static byte [] bitmapToByte (Bitmap imgBitmap, int quality, Bitmap.CompressFormat format){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imgBitmap.compress(format, quality, baos);
        return baos.toByteArray();
    }


    /**
     * Mostra uma mensagem customizada para o usuário
     * @param message Mensagem a ser mostrada pelo Toast
     * @param type indica o tipo do toast 1 para erro 2 para aviso
     */
    public static void showCustomizedToast(Context context, int type, String message, View toastStyle){
        Toast toast = new Toast(context);
        ImageView toastImg = toastStyle.findViewById(R.id.toast_type);
        TextView toastText = toastStyle.findViewById(R.id.message);
        toastText.setText(message);
        modifyToast(context, type, toastStyle.findViewById(R.id.container_toast), toastImg);
        toast.setView(toastStyle);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Modifica a estrutura de um toast
     * @param type Indica o tipo do toast, 1 para erro 2 para aviso
     * @param toastContainer referencia para o layout do toast
     * @param toastImg referencia para a imagem do toast
     */
    private static void modifyToast(Context context, int type, View toastContainer, ImageView toastImg){
        switch (type){
            case 1:
                toastContainer.setBackground(context.getDrawable(R.drawable.toast_error));
                toastImg.setImageResource(R.drawable.ic_baseline_error_24);
                break;
            case 2:
                toastContainer.setBackground(context.getDrawable(R.drawable.toast_warning));
                toastImg.setImageResource(R.drawable.ic_baseline_warning_24);
                break;
        }
    }

}
