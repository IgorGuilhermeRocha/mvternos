package com.igor.mvternos.domain.util;

/**
 * @author Igor Guilherme Almeida Rocha
 * Classe que implementa alguns métodos para a formatação, como preço.
 * Ex -> 1900.05 = R$ 1.900,05
 */
public class FormatUtil {

    public static String formatPrice(Double price){

        int absolutValue = (int) Math.floor(price);
        String cents = String.format("%.2f", price - absolutValue);
        String formattedPrice = "R$ " + applyDots(String.valueOf(absolutValue));
        return String.format(formattedPrice + "," + cents.replaceAll("0,",""));
    }


    private static String applyDots(String value){
        String newString = "";
        for(int i = 0; i < value.length() ; i++){
            newString += value.charAt(i)+"";
            if((value.length() - (i + 1)) % 3 == 0 && (value.length() - (i + 1)) != 0){
                newString += ".";
            }
        }
        return newString;
    }
}
