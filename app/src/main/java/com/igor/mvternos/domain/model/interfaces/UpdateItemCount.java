package com.igor.mvternos.domain.model.interfaces;

public interface UpdateItemCount {

    void updateCount(long count);
}
