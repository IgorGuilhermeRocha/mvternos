package com.igor.mvternos.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.igor.mvternos.config.FirebaseConfig;

public class StockRepository {

    public static DatabaseReference database;
    private static final String STOCK_REFERENCE = "suit_stock";

    public static void getStock(ValueEventListener listener, String suitId){

        if(database == null){
            database = FirebaseConfig.getDatabaseReference();
        }

        DatabaseReference databaseAux = database.child(STOCK_REFERENCE+"/"+suitId);

        databaseAux.addValueEventListener(listener);

    }
}
