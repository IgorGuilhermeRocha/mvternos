package com.igor.mvternos.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.igor.mvternos.config.FirebaseConfig;

/**
 * @author Igor Guilherme Almeida Rocha
 * Classe que faz algumas operações relacionadas as consultas de "suits" no firebase
 */
public class SuitRepository {

    public static DatabaseReference database;

    public static void getAllSuits(ValueEventListener listener){

        if(database == null){
            database = FirebaseConfig.getDatabaseReference();
        }

        DatabaseReference databaseAux = database.child("suit");
        databaseAux.addValueEventListener(listener);
    }

}
