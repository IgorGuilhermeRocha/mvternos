package com.igor.mvternos.provider;


import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.domain.model.Order;
import com.igor.mvternos.domain.model.interfaces.UpdateItemCount;
import com.igor.mvternos.domain.model.interfaces.UpdateOrders;

import java.util.ArrayList;
import java.util.List;

public class OrderProvider {

    private static DatabaseReference databaseReference;
    private static UpdateItemCount updateItemCount;
    private static UpdateOrders updateOrders;

    public static void getCountOfOrders(){
        DatabaseReference aux = getShoppingCartPath();

        aux.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                updateItemCount.updateCount(snapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public static void getOrders(){

        DatabaseReference aux = getShoppingCartPath();
        aux.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Order> orders =  new ArrayList<>();
                for(DataSnapshot snap : snapshot.getChildren()){
                    orders.add(snap.getValue(Order.class));
                }
                updateOrders.updateListOfOrders(orders);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    public static void removeOrder(Order order){
        DatabaseReference aux = getShoppingCartPath();
        aux.child(order.getId()).removeValue();
        SuitStockProvider.addStock(order);
    }

    private static DatabaseReference getShoppingCartPath(){
        if(databaseReference == null) databaseReference = FirebaseConfig.getDatabaseReference();
         return databaseReference.child("user/" +
                FirebaseConfig.getFirebaseAuth().getCurrentUser().getUid()+"/shopping_cart");

    }

    public static void setUpdateOrders(UpdateOrders updateOrders){
        OrderProvider.updateOrders = updateOrders;
    }
    public static void setUpdateItemCount(UpdateItemCount updateItemCount) {
        OrderProvider.updateItemCount = updateItemCount;
    }
}
