package com.igor.mvternos.provider;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.domain.model.Order;

public class SuitStockProvider {

    private static DatabaseReference databaseReference;

    public static void removeStock(Order order){

        DatabaseReference aux  = pathStock(order.getSuit().getId());
        Task<DataSnapshot> task = aux.child(order.getSize().toString()).get();
        task.addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if(task.isSuccessful()){
                    Long currentQuantity = (Long) task.getResult().getValue();
                    aux.child(order.getSize().toString()).setValue(currentQuantity - order.getQuantity());
                }
            }
        });


    }

    public static void addStock(Order order){
        DatabaseReference aux = getStockPath(order.getSuit().getId());
        final String path = "/" + order.getSize().name();
        aux.child(path).get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if(task.isSuccessful()){
                    Long quantity =  (Long) task.getResult().getValue();
                    quantity += order.getQuantity();
                    aux.child(path).setValue(quantity);

                }
            }
        });

    }

    private static DatabaseReference getStockPath(String id){
        if(databaseReference == null) databaseReference = FirebaseConfig.getDatabaseReference();
        return databaseReference.child("suit_stock/" + id );
    }


    private static DatabaseReference pathStock(String suitId){
        if(databaseReference == null) databaseReference = FirebaseConfig.getDatabaseReference();
        return databaseReference.child("suit_stock/" + suitId);
    }
}
