package com.igor.mvternos.config;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Classe reponsável por gerenciar as instãncias das classes que fazem acesso ao bando do firebase
 * @author Igor Guilherme Almeida Rocha
 */
public class FirebaseConfig {

    private static FirebaseAuth firebaseAuth;
    private static DatabaseReference databaseReference;
    private static StorageReference firebaseStorage;
    private static FirebaseFirestore firebaseFirestore;


    /**
     * @return Retorna uma instância de FirebaseAuth, para cadastro, login e outras operações com o usuário
     */
    public static FirebaseAuth getFirebaseAuth(){
        if(firebaseAuth == null){
            firebaseAuth = FirebaseAuth.getInstance();
        }
        return firebaseAuth;
    }

    /**
     * @return retorna uma referencia para o banco de imagens
     */
    public static StorageReference getFirebaseStorageReference(){
        if(firebaseStorage == null){
            firebaseStorage = FirebaseStorage.getInstance().getReference();
        }
        return firebaseStorage;
    }

    /**
     * @return retorna referencia para o banco de dados
     */
    public static DatabaseReference getDatabaseReference(){
        if(databaseReference == null){
            databaseReference = FirebaseDatabase.getInstance().getReference();
        }
        return databaseReference;
    }

    public static FirebaseFirestore getFirebaseFirestoreReference(){
        if(firebaseFirestore == null){
            firebaseFirestore = FirebaseFirestore.getInstance();
        }
        return firebaseFirestore;
    }


}
