package com.igor.mvternos.config;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Classe que retorna os objetos que fazer acesso e leitura ao arquivo de preferencias
 * @author Igor Guilherme Almeida Rocha
 */
public class SharedPreferencesConfig {

    public static final Integer PRIVATE = 0;
    public static final String USER_CREDENTIALS = "USER CREDENTIALS";

    public static SharedPreferences.Editor getSharedEditor(Context context, final String ARCHIVE, final int PERMISSIONS){
            return context.getSharedPreferences(ARCHIVE, PERMISSIONS).edit();
    }

    public static SharedPreferences getSharedRead(Context context, final String ARCHIVE, final int PERMISSIONS){
        return context.getSharedPreferences(ARCHIVE, PERMISSIONS);
    }
}
