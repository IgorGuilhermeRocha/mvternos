package com.igor.mvternos.activity;

import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.igor.mvternos.R;
import com.igor.mvternos.ui.register.RegisterFragment1;

/**
 * Classe reponsável por representar a activity de cadastro
 * @author Igor Guilherme Almeida Rocha
 */
public class RegisterActivity extends AppCompatActivity {

    private RegisterFragment1 registerFragment1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_register);

        registerFragment1 = new RegisterFragment1();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.register_content, registerFragment1);
        transaction.commit();
    }


}