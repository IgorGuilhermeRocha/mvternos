package com.igor.mvternos.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.igor.mvternos.R;
import com.igor.mvternos.domain.util.DecodeAndEncodeUtil;


public class teste_activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teste);
        String aux = DecodeAndEncodeUtil.encodeToBase64("123");
        Log.d("debugdecode", DecodeAndEncodeUtil.encodeToBase64("123"));
        Log.d("debugdecode", DecodeAndEncodeUtil.decodeBase64(aux));


    }
}