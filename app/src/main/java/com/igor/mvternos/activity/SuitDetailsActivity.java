package com.igor.mvternos.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.igor.mvternos.R;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.controller.SuitDetailsActivityController;
import com.igor.mvternos.controller.SuitsFragmentController;
import com.igor.mvternos.domain.model.Suit;
import com.igor.mvternos.domain.model.enums.SuitSize;
import com.igor.mvternos.domain.util.FormatUtil;
import com.igor.mvternos.repository.StockRepository;
import com.igor.mvternos.ui.dialogs.SuitOutDialogFragment;

import java.util.ArrayList;
import java.util.HashMap;

public class SuitDetailsActivity extends AppCompatActivity {

    private Suit suit;
    private ImageView suitImg;
    private TextView tvTitle;
    private TextView tvPrice;
    private TextView tvDescription;
    private Button btAddToCart;
    private Spinner spinnerSize;
    private Spinner spinnerQuantity;
    private HashMap<String, Long> stock;
    private ArrayAdapter<String> sizes;
    private ArrayAdapter<Long> quantity;
    private SuitDetailsActivityController controller;

    private ValueEventListener stockListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suit_details);
        initialConfig();

    }


    /**
     * Método que faz as configurações iniciais desta activity
     */
    private void initialConfig(){
        this.controller = new SuitDetailsActivityController();
        getSupportActionBar().hide();
        initComponents();
        this.suit = (Suit) getIntent().getExtras().get("suit");
        setListeners();
        loadImg();
        getStock();
        setEvents();
        dataBinding();
    }


    /**
     * Inicializa componenetes
     */
    private void initComponents(){
        this.tvTitle = findViewById(R.id.tv_title);
        this.tvPrice = findViewById(R.id.tv_price);
        this.spinnerSize= findViewById(R.id.spinner_size);
        this.spinnerQuantity = findViewById(R.id.spinner_quantity);
        this.btAddToCart = findViewById(R.id.bt_add_to_cart);
        this.suitImg = findViewById(R.id.iv_suit);
        this.tvDescription = findViewById(R.id.tv_description);
    }

    /**
     * Carrega a imagem do terno
     */
    private void loadImg(){
        SuitsFragmentController.getSuitList().forEach(
                (suit) -> {
                    if(suit.equals(this.suit)){
                        this.suitImg.setImageBitmap(suit.getImgBitmap());
                    }
                }
        );
    }

    /**
     * Configura o listener reponsável por "ouvir o firebase" e popular os spinners
     * @see StockRepository
     */
    private void setListeners(){
        this.stockListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                fillStock(snapshot);
                int position = spinnerSize.getSelectedItemPosition();
                if(position >= 0){
                    configSizeSpinner(spinnerSize.getSelectedItemPosition());
                }else{
                    configSizeSpinner();
                }

                configQuantitySpinner();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

    }

    /**
     * Este métoto é reponsável por pupular o estoque
     * @param snapshot Objeto que da acesso aos valores recuperados do banco
     */
    private void fillStock(DataSnapshot snapshot){
        this.stock = new HashMap<>();
        this.stock.put("XG", (Long) snapshot.child("XG").getValue());
        this.stock.put("GG", (Long) snapshot.child("GG").getValue());
        this.stock.put("G", (Long) snapshot.child("G").getValue());
        this.stock.put("M", (Long) snapshot.child("M").getValue());
        this.stock.put("P", (Long) snapshot.child("P").getValue());
    }

    /**
     * Método que preenche o spinner relacionado aos tamanhos
     */
    private void configSizeSpinner(){
        this.sizes = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1, new ArrayList<>(stock.keySet()));
        this.spinnerSize.setAdapter(sizes);
        this.spinnerSize.setSelection(3);

    }

    /**
     *  Método que preenche o spinner relacionado aos tamanhos
     * @param position Posição atual do spinner
     */
    private void configSizeSpinner(int position){
        this.sizes = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1, new ArrayList<>(stock.keySet()));
        this.spinnerSize.setAdapter(sizes);
        this.spinnerSize.setSelection(position);

    }

    /**
     * Método que popula o spinner relacionado a quantidade de ternos disponíveis
     */
    private void configQuantitySpinner(){
        this.quantity = new ArrayAdapter<Long>(getApplicationContext(), android.R.layout.simple_list_item_1, getQuantityList(stock.get(this.spinnerSize.getSelectedItem().toString())));
        if(this.quantity.getCount() == 0){
            this.quantity.add(0L);
        }
        this.spinnerQuantity.setAdapter(quantity);
    }

    /**
     * Método que gera os valores do spinner de quantidade
     * @param maxQuantity Quantidade  no estoque
     * @return Um ArrayList com os valores que devem ser exibidos pelo spinner de quantidade
     * se maxQuantity == 6 o retorno é 1,2,3,4,5,6 se maxQuantity == 0 então retorna uma lista vazia
     */
    private ArrayList<Long> getQuantityList(Long maxQuantity){
        ArrayList<Long> listValues = new ArrayList<>();
        if(maxQuantity != 0 )
            for(long i = 1; i <= maxQuantity; i++) listValues.add(i);


        return listValues;
    }

    /**
     * Método que seta o listener de estoque
     */
    private void getStock(){
        StockRepository.getStock(this.stockListener, this.suit.getId());
    }

    /**
     * Método que seta os eventos
     */
    private void setEvents(){
        setSpinnerEvents();
        setButtonEvents();
    }

    /**
     * Método que implementa os eventos de spinner
     */
    private void setSpinnerEvents(){
        this.spinnerSize.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            configQuantitySpinner();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );


    }

    /**
     * Seta os eventos de botao
     */
    private void setButtonEvents(){
        this.btAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((long) spinnerQuantity.getSelectedItem() == 0L){
                   var dialog = new SuitOutDialogFragment();
                   dialog.show(getSupportFragmentManager(), dialog.getTag());
                }else{
                    controller.addOrderToCart(suit, (Long) spinnerQuantity.getSelectedItem(),
                            SuitSize.valueOf(spinnerSize.getSelectedItem().toString()));
                }
            }
        });
    }

    /**
     * Este método pega o terno selecionado e seta os campos com os valores do mesmo
     */
    private void dataBinding(){
        this.tvTitle.setText(suit.getName());
        this.tvPrice.setText(FormatUtil.formatPrice(suit.getValue()));
        this.tvDescription.setText(suit.getDescription());
    }



}