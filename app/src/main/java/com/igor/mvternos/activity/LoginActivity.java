package com.igor.mvternos.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;

import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.igor.mvternos.R;
import com.igor.mvternos.controller.LoginActivityController;
import com.igor.mvternos.domain.util.Util;

import java.util.HashMap;

/**
 * Classe que representa a activity de login
 * @author Igor Guilherme Almeida Rocha
 */
public class LoginActivity extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword;

    private Button btLogIn;
    private Button btRegister;
    private Button btHideShow;

    private CheckBox cbRemember;

    private Boolean isHide = true;

    private LoginActivityController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialConfig();
    }

    /**
     * Faz as configurações iniciais
     */
    private void initialConfig(){
        initComponents();
        setEvents();
        controller = new LoginActivityController(this, View.inflate(this, R.layout.activity_login, new LinearLayoutCompat(this)));
        setPrompts();
    }

    /**
     * Inicializa os componentes de tela
     */
    private void initComponents(){
        this.etEmail = findViewById(R.id.et_email);
        this.etPassword = findViewById(R.id.et_password);
        this.btLogIn = findViewById(R.id.bt_login);
        this.btRegister = findViewById(R.id.bt_register);
        this.btHideShow = findViewById(R.id.bt_hide_show);
        this.cbRemember = findViewById(R.id.cb_remember);
    }

    /**
     * seta os eventos
     */
    private void setEvents(){
        btLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        btHideShow.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) { isHide = hideShow(btHideShow, etPassword, isHide); }
        });
    }

    /**
     * método para fazer o logIn
     */
    private void logIn(){
        String email = this.etEmail.getText().toString();
        String password = this.etPassword.getText().toString();
        this.controller.login(email, password, this.cbRemember.isChecked());
    }

    /**
     * vai para a tela de cadastro
     */
    private void register(){
        this.controller.register();
    }

    /**
     * Método que verifica se a senha esta visivel ou nao
     */
    private Boolean hideShow(Button btHideShow, EditText etPassword, boolean isHide){
        TransformationMethod method;
        Integer imgId;

        if(isHide) {
            method = HideReturnsTransformationMethod.getInstance();
            imgId  = R.drawable.eyeshow_removebg_preview;
        }
        else {
            method = PasswordTransformationMethod.getInstance();
            imgId = R.drawable.eye_removebg_preview;
        }

        return Util.transformEditText(this, btHideShow, etPassword, method, imgId, isHide);
    }

    /**
     * Seta os valores dos campos email, senha lembrar-me, caso o usuário tenha
     * pedido para ser lembrado
     */
    private void setPrompts(){
        HashMap<String, String> credentials = this.controller.getCredentials();
        this.etEmail.setText(credentials.get("email"));
        this.etPassword.setText(credentials.get("password"));
        this.cbRemember.setChecked(Boolean.valueOf(credentials.get("isChecked")));
    }


}