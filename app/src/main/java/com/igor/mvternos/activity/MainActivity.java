package com.igor.mvternos.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.igor.mvternos.R;
import com.igor.mvternos.databinding.ActivityMainBinding;
import com.igor.mvternos.domain.model.interfaces.SendMessageWhatsApp;
import com.igor.mvternos.ui.adjustments.AdjustmentsFragment;


/**
 * Classe reponsável por representar a activity principal
 * @author Igor Guilherme Almeida Rocha
 */
public class MainActivity extends AppCompatActivity implements SendMessageWhatsApp {


    private final String WHATS_APP_URI = "https://api.whatsapp.com/send?phone=%s&text=%s";
    private final String MV_TERNOS_PHONE_NUMBER = "+55 31 98041-0330";

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AdjustmentsFragment.setProvider(this);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBarMain.toolbar);
        binding.appBarMain.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               sendMessage("  ");
            }
        });

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;

        // setting color of navigation menu
        navigationView.setBackgroundResource(R.color.hint_color);

        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_home, R.id.nav_suits, R.id.nav_adjustments,
                R.id.nav_clients, R.id.nav_contact, R.id.nav_about_us)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //Toast.makeText(this, item.getItemId()+"dsfdsf", Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    public void sendMessage(String text){
        Intent toWhatsapp = new Intent(Intent.ACTION_VIEW);
        toWhatsapp.setData(Uri.parse(String.format(WHATS_APP_URI, MV_TERNOS_PHONE_NUMBER, text)));
        startActivity(toWhatsapp);
    }
}