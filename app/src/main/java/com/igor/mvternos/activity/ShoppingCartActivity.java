package com.igor.mvternos.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.igor.mvternos.R;
import com.igor.mvternos.adapters.ItemCartAdapter;
import com.igor.mvternos.adapters.SwipeCallback;
import com.igor.mvternos.domain.model.Order;
import com.igor.mvternos.domain.model.interfaces.UpdateOrders;
import com.igor.mvternos.provider.OrderProvider;

import java.util.List;

public class ShoppingCartActivity extends AppCompatActivity implements UpdateOrders {

    private RecyclerView rvOrders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        this.rvOrders = findViewById(R.id.rv_orders);
        OrderProvider.setUpdateOrders(this);
        OrderProvider.getOrders();

    }

    @Override
    public void updateListOfOrders(List<Order> orders) {
        ItemCartAdapter adapter = new ItemCartAdapter(orders);
        rvOrders.setAdapter(adapter);
        rvOrders.setHasFixedSize(true);
        rvOrders.setLayoutManager(new LinearLayoutManager(this));
        ItemTouchHelper touchHelper = new ItemTouchHelper(new SwipeCallback(adapter));
        touchHelper.attachToRecyclerView(rvOrders);
    }
}