package com.igor.mvternos.adapters;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.igor.mvternos.R;
import com.igor.mvternos.domain.model.Suit;
import com.igor.mvternos.domain.model.interfaces.RecyclerSuitOnClick;
import com.igor.mvternos.domain.util.FormatUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Classe adapter da lista de ternos
 * @author Igor Guilherme Almeida Rocha
 */
public class SuitAdapter extends RecyclerView.Adapter<SuitAdapter.SuitHolder> implements Filterable {

    private final RecyclerSuitOnClick onClickAction;
    private Context context;
    private List<Suit> listSuitFull;
    private List<Suit> listSuit;
    private String pricePrompt;

    public SuitAdapter(List<Suit> list, Context context, RecyclerSuitOnClick onClickAction){
        this.context  = context;
        this.listSuit = new ArrayList<>(list);
        this.listSuitFull = new ArrayList<>(list);
        if(this.context.getResources() != null){
            this.pricePrompt =  context.getResources().getString(R.string.suit_price);
        }
        this.onClickAction = onClickAction;

    }


    /**
     * Filtro de ternos baseado no nome digitado pelo usuário
     */
    private Filter filterSuits = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Suit> filteredList = new ArrayList<>();

            if(constraint == null || constraint.toString().trim().length() == 0){
                filteredList.addAll(listSuitFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(Suit suit : listSuitFull){
                    if(suit.getName().toLowerCase().contains(filterPattern)){
                        filteredList.add(suit);
                    }
                }
            }

            FilterResults results  = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listSuit.clear();
            listSuit.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };



    @NonNull
    @Override
    public SuitHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.suits_list_item, parent, false);
        return new SuitHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull SuitHolder holder, int position) {
        Suit suit = this.listSuit.get(position);
        holder.tvName.setText(suit.getName());
        holder.tvPrice.setText(pricePrompt + " " + FormatUtil.formatPrice(suit.getValue()));
        holder.ivSuit.setImageBitmap(suit.getImgBitmap());

    }

    @Override
    public int getItemCount() {
        return this.listSuit.size();
    }

    @Override
    public Filter getFilter() {
        return filterSuits;
    }

    public class SuitHolder extends RecyclerView.ViewHolder {

        public ImageView ivSuit;
        public TextView tvName;
        public TextView tvPrice;

        public SuitHolder(@NonNull View itemView) {
            super(itemView);


            this.ivSuit = itemView.findViewById(R.id.iv_suit_img);
            this.tvName = itemView.findViewById(R.id.tv_name);
            this.tvPrice = itemView.findViewById(R.id.tv_price);

            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if( onClickAction != null  &&  getAbsoluteAdapterPosition() != RecyclerView.NO_POSITION){
                                onClickAction.onItemClicked(listSuit.get(getAbsoluteAdapterPosition()));
                            }
                        }
                    }
            );

        }
    }
}
