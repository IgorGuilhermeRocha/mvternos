package com.igor.mvternos.adapters;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.motion.widget.OnSwipe;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.igor.mvternos.R;
import com.igor.mvternos.domain.model.Order;
import com.igor.mvternos.domain.util.FormatUtil;
import com.igor.mvternos.provider.OrderProvider;

import java.util.List;

/**
 * Adpter da lista de pedidos do carrinho
 * @author Igor Guilherme Almeida Rocha
 */
public class ItemCartAdapter extends RecyclerView.Adapter<ItemCartAdapter.ItemCartHolder> {

    private List<Order> orders;

    public ItemCartAdapter(List<Order> orders){
        this.orders = orders;
    }


    @NonNull
    @Override
    public ItemCartAdapter.ItemCartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.shopping_cart_item, parent, false);
        return new ItemCartHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemCartAdapter.ItemCartHolder holder, int position) {
        Order orderAux = this.orders.get(position);
        holder.tvName.setText(orderAux.getSuit().getName());
        holder.tvQuantity.setText("Quantidade: " + orderAux.getQuantity()+"");
        holder.tvSize.setText(orderAux.getSize().toString());
        holder.tvPrice.setText(FormatUtil.formatPrice(orderAux.getPrice()));
    }

    @Override
    public int getItemCount() {
        return this.orders.size();
    }

    public static class ItemCartHolder extends RecyclerView.ViewHolder  {

        public TextView tvName;
        public TextView tvQuantity;
        public TextView tvPrice;
        public TextView tvSize;

        public ItemCartHolder(@NonNull View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.tv_name);
            this.tvQuantity = itemView.findViewById(R.id.tv_quantity);
            this.tvSize = itemView.findViewById(R.id.tv_size);
            this.tvPrice = itemView.findViewById(R.id.tv_price);

        }


    }

    public void removeItem(int position){
        OrderProvider.removeOrder(this.orders.get(position));
    }


}
