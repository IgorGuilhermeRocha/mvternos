package com.igor.mvternos.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.igor.mvternos.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

/**
 * Adapter do slider de imagens
 * @author Igor Guilherme Almeida Rocha
 */
public class SliderAdapter extends SliderViewAdapter<SliderAdapter.Holder> {

    public int[] images;


    public SliderAdapter(int [] images){
        this.images = images;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_img, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder viewHolder, int position) {
        viewHolder.ivImage.setImageResource(this.images[position]);
    }

    @Override
    public int getCount() {
        return this.images.length;
    }

    public class Holder extends SliderViewAdapter.ViewHolder{
        ImageView ivImage;

        public Holder(View itemView){
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_current_img);

        }
    }
}
