package com.igor.mvternos.ui.register;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import com.igor.mvternos.R;
import com.igor.mvternos.domain.model.User;
import com.igor.mvternos.domain.util.UserPermissions;
import com.igor.mvternos.domain.util.Util;

import java.io.IOException;

/**
 * Classe que representa a segunda parte da activity de registro
 * @author Igor Guilherme Almeida Rocha
 */
public class RegisterFragment2  extends Fragment {

    private final String[] PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private final Integer PERMISSION_REQUEST_CODE = 2;

    private ImageView ivPicture;
    private User user;

    private RegisterFragment1 registerFragment1;
    private RegisterFragment3 registerFragment3;

    private Button btNext;
    private Button btBack;
    private Button btTakePicture;
    private Button btSelectPicture;

    /**
     * Listener que vai tratar o resultado da intent de selcionar / tirar foto
     */
    ActivityResultLauncher<Intent> activityResultLaunch = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {

                @Override
                public void onActivityResult(ActivityResult result) {
                   Intent resultData = result.getData();

                    if(result.getResultCode() == -1) {
                        try {
                            Bitmap imgBitMap = (Bitmap) resultData.getExtras().get("data");
                            setBitMap(imgBitMap);
                        }catch (NullPointerException e){
                            Uri selectedImage = resultData.getData();
                            try {
                                Bitmap imgBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                                setBitMap(imgBitmap);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                        }
                    }


                }
            });

    public RegisterFragment2(){

    }

    public RegisterFragment2(User user){
        this.user = user;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_register_2, container, false);
        initComponents(view);
        setImg();
        setEvents();
        return view;
    }

    /**
     * Inicializa os componentes
     * @param view view inflada
     */
    private void initComponents(View view){
        this.ivPicture = view.findViewById(R.id.iv_picture);
        this.btNext = view.findViewById(R.id.bt_next);
        this.btBack = view.findViewById(R.id.bt_back);
        this.btTakePicture = view.findViewById(R.id.bt_take_picture);
        this.btSelectPicture = view.findViewById(R.id.bt_gallery);
    }

    /**
     * Seta o ImageView com o bitmap do usuario ou uma imagem vazia
     */
    private void setImg(){
        if(user.getImgBitmap()  == null) this.ivPicture.setImageResource(R.drawable.emptyuser);
        else this.ivPicture.setImageBitmap(user.getImgBitmap());
    }

    /**
     * Seta os eventos
     */
    private void setEvents(){
        this.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { previousFragment(); }
        });

        this.btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { nextFragment(); }
        });

        this.btTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { takePicture(); }
        });

        this.btSelectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { selectPicture(); }
        });

    }

    /**
     * Vai para o fragment anterior
     */
    private void previousFragment(){
        registerFragment1 = new RegisterFragment1(this.user);
        Util.changeFragments(registerFragment1, getActivity(), R.id.register_content);
    }

    /**
     * Vai para o próximo fragment
     */
    private void nextFragment(){
        registerFragment3 = new RegisterFragment3(this.user);
        Util.changeFragments(registerFragment3, getActivity(), R.id.register_content);
    }

    /**
     * Abre a camera
     */
    private void takePicture(){
        if(UserPermissions.verifyPermissions(getActivity(), PERMISSIONS, PERMISSION_REQUEST_CODE)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            activityResultLaunch.launch(takePictureIntent);
        }
    }

    /**
     * Abre a galeria
     */
    private void selectPicture(){
        if(UserPermissions.verifyPermissions(getActivity(), PERMISSIONS, PERMISSION_REQUEST_CODE)) {
            Intent selectPicture = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            activityResultLaunch.launch(selectPicture);
        }
    }

    /**
     * Seta a imagem do ImageView e do usuário
     * @param imgBitmap Imagem em bitmap
     */
    private void setBitMap(Bitmap imgBitmap){
        ivPicture.setImageBitmap(imgBitmap);
        user.setImgBitmap(imgBitmap);
    }


}
