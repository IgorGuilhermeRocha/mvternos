package com.igor.mvternos.ui.contacts;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.igor.mvternos.R;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

/**
 * Classe que representa a tela de contatos
 * @link https://github.com/medyo/android-about-page
 * @autor Igor Guilherme Almeida Rocha
 */
public class ContactsFragment extends Fragment {

    public ContactsFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = new AboutPage(getContext(), R.style.Widget_App_AboutPage)
                .isRTL(false)
                .addGroup("Empresa")
                .addEmail("mvdigital@gmail.com", "Email de contato")
                .addInstagram("mv.ternos","Instagram")
                .addWebsite("https://mvternos.com","Acesse nosso site")
                .addItem(builderElement("https://www.youtube.com/@mvternos5575"
                        , "Veja as novidades em nosso canal !", R.drawable.youtube, R.color.red))
                .addGroup("Desenvolvedor")
                .addItem(builderElement("https://www.linkedin.com/in/igor-guilherme-945525214", "Currículo no Linkedin !", R.drawable.linkedin, R.color.main_blue))
                .addItem(builderElement("https://gitlab.com/IgorGuilhermeRocha","Repositório de projetos", R.drawable.gitlab, R.color.gitlab_orange))
                .addEmail("igorguilhermedev@gmail.com", "Email")
                .addInstagram("igor_guilherme_rocha","Instagram")
                .addWebsite("https://personal-website-igor-rocha.netlify.app","Portfólio")
                .create();

        view.findViewById(mehdi.sakout.aboutpage.R.id.image).setVisibility(View.GONE);
        view.findViewById(mehdi.sakout.aboutpage.R.id.description_separator).setVisibility(View.GONE);
        view.findViewById(mehdi.sakout.aboutpage.R.id.description).setVisibility(View.GONE);


        return view;
    }

    /**
     * Cria e retorna um Element
     * @param uri String que representa url
     * @param title Título do item
     * @param drawableId Id do icone do item
     * @param color cor do icone
     * @return Um Element
     */
    private Element builderElement(String uri, String title,  int drawableId, int color ){

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse(uri));

        Element element = new Element();
        element.setIconDrawable(drawableId);
        element.setTitle(title);
        element.setIconTint(color);
        element.setIntent(intent);

        return element;

    }
}