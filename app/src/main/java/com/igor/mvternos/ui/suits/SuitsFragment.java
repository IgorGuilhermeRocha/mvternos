package com.igor.mvternos.ui.suits;


import android.content.Intent;
import android.os.Bundle;


import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;


import com.igor.mvternos.R;
import com.igor.mvternos.activity.ShoppingCartActivity;
import com.igor.mvternos.controller.SuitsFragmentController;


/**
 * Classe que representa a tela de lista de ternos
 * @author Igor Guilherme Almeida Rocha
 */
public class SuitsFragment extends Fragment {


    private SuitsFragmentController controller;
    private ImageView ivClear;
    private ImageView ivSearch;
    private ImageView ivCart;
    private EditText etSearch;


    public SuitsFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_suits, container, false);
        initComponents(view);
        initController(view);
        updateList();
        setEvents();
        return view;
    }

    /**
     * Inicializa os componentes de tela
     * @param view view inflada
     */
    private void initComponents(View view){
        this.ivSearch = view.findViewById(R.id.iv_search);
        this.ivClear = view.findViewById(R.id.iv_clear);
        this.ivCart = view.findViewById(R.id.iv_cart);
        this.etSearch = view.findViewById(R.id.et_search);

    }

    /**
     * Inicializa o controller
     * @param view
     */
    private void initController(View view){
        this.controller = new SuitsFragmentController(getContext(), view);
    }

    /**
     * Método que atualiza a lista de ternos
     */
    private void updateList(){
        this.controller.updateList();
    }

    /**
     * Seta os eventos
     */
    private void setEvents(){
        setIvEvents();
        setEtEvents();
    }

    /**
     * Seta os eventos de providos de um ImageView
     */
    private void setIvEvents(){
        this.ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { openCloseSearchView(); }
        });

        this.ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { clearAction(); }
        });

        this.ivCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { toCart(); }
        });
    }

    /**
     * Seta os eventos providos por um EditText
     */
    private void setEtEvents(){
        this.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) { controller.search(s.toString()); }
        });
    }

    /**
     * Este método redireciona o usuário para a tela dos itens no carrinho
     */
    private void toCart(){
        Intent intent = new Intent(getContext(), ShoppingCartActivity.class);
        startActivity(intent);
    }

    /**
     * Método que limpa o texto na barra de pesquisa e fecha deixa a mesma invisivel
     */
    private void clearAction(){
        if(!this.etSearch.getText().toString().replaceAll(" ","").equals("")){
            this.etSearch.setText("");
        }
        openCloseSearchView();
    }

    /**
     * Muda a visibilidade dos componentes que fazem parte da barra de pesquisa
     */
    private void openCloseSearchView() {
        this.etSearch.setVisibility(this.etSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        this.ivSearch.setVisibility(this.ivSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        this.ivClear.setVisibility(this.ivClear.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }


}