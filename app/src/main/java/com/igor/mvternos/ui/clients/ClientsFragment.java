package com.igor.mvternos.ui.clients;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.igor.mvternos.R;
import com.igor.mvternos.adapters.SliderAdapter;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

/**
 * Classe que representa o slider de clients
 * @see SliderAdapter
 * @author Igor Guilherme Almeida Rocha
 */
public class ClientsFragment extends Fragment{

    private SliderView svSlider;

    private final int[] imgIds = {
            R.drawable.clients_1,
            R.drawable.clients_2,
            R.drawable.clients_3,
            R.drawable.clients_4,
            R.drawable.clients_5,
    };

    public ClientsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_clients, container, false);
        initComponents(view);
        configAdapter();
        return view;
    }


    /**
     * Inicializa os componetes
     * @param view
     */
    private void initComponents(View view){
        this.svSlider = view.findViewById(R.id.img_slider);
    }


    /**
     * Faz as configurações necessárias para o adapter
     */
    private void configAdapter(){
        SliderAdapter adapter = new SliderAdapter(this.imgIds);
        this.svSlider.setSliderAdapter(adapter);
        this.svSlider.setIndicatorAnimation(IndicatorAnimationType.WORM);
        this.svSlider.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION);
        this.svSlider.startAutoCycle();
    }
}