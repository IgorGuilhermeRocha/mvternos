package com.igor.mvternos.ui.register;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.igor.mvternos.R;
import com.igor.mvternos.controller.RegisterFragment3Controller;
import com.igor.mvternos.domain.model.User;
import com.igor.mvternos.domain.util.Util;

/**
 * Classe que representa última parte da activity de registro
 * @author Igor Guilherme Almeida Rocha
 */
public class RegisterFragment3 extends Fragment {

    private User user;

    // Atributos que representam a visibilidade do campo de senha
    private Boolean isHide = true;
    private Boolean isHide2 = true;

    private Button btBack;
    private Button btConfirmRegister;
    private Button btHideShow;
    private Button btHideShow2;

    private EditText etEmail;
    private EditText etPassword;
    private EditText etConfirmPassword;

    private RegisterFragment3Controller controller;

    public RegisterFragment3() {

    }

    public RegisterFragment3(User user) {
        this.user = user;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_3, container, false);
        intiController(view);
        initComponents(view);
        setEvents();
        setPrompts();
        return view;
    }

    /**
     * Inicia o controlador
     * @param view View inflada
     */
    private void intiController(View view){
        if(this.controller == null){
            controller = new RegisterFragment3Controller(getContext(), view);
        }
    }

    /**
     * Inicia os componentes
     * @param view View inflada
     */
    private void initComponents(View view){
        this.btHideShow = view.findViewById(R.id.bt_hide_show);
        this.btHideShow2 = view.findViewById(R.id.bt_hide_show_2);
        this.btBack = view.findViewById(R.id.bt_back);
        this.btConfirmRegister = view.findViewById(R.id.bt_confirm_form);
        this.etEmail = view.findViewById(R.id.et_email);
        this.etPassword =  view.findViewById(R.id.et_password);
        this.etConfirmPassword =  view.findViewById(R.id.et_confirm_password);
    }

    /**
     * Seta os eventos
     */
    private void setEvents(){
        this.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { previousFragment(); }
        });

        this.btConfirmRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { registerUser(); }
        });

        this.btHideShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHide = hideShow(btHideShow, etPassword, isHide);
            }
        });

        this.btHideShow2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { isHide2 = hideShow(btHideShow2, etConfirmPassword, isHide2); }
        });
    }

    /**
     * Vai para o fragment anterior
     */
    private void previousFragment(){
        buildUser();
        Util.changeFragments(new RegisterFragment2(user), getActivity(), R.id.register_content);
    }

    /**
     * valida os campos e registra o usuário
     */
    private void registerUser(){
        if(validForm()){
            buildUser();
            this.controller.registerUser(this.user);
        }
    }

    /**
     * Valida os campos de email, senha e confirmar senha
     * @return true, se tudo estiver preenchido corretamente
     */
    private Boolean validForm(){
        return this.controller.validForm(this.etEmail.getText().toString(),
                this.etPassword.getText().toString(),
                this.etConfirmPassword.getText().toString());

    }

    /**
     * seta o email e a senha na instancia de usuario
     */
    private void buildUser(){
        this.user.setEmail(this.etEmail.getText().toString());
        this.user.setPassword(this.etPassword.getText().toString());
    }

    /**
     * Método que verifica se a senha esta visivel ou nao
     */
    private Boolean hideShow(Button btHideShow, EditText etPassword, boolean isHide){
        TransformationMethod method;
        Integer imgId;

        if(isHide) {
            method = HideReturnsTransformationMethod.getInstance();
            imgId  = R.drawable.eyeshow_removebg_preview;
        }
        else {
            method = PasswordTransformationMethod.getInstance();
            imgId = R.drawable.eye_removebg_preview;
        }

        return Util.transformEditText(getActivity(), btHideShow, etPassword, method, imgId, isHide);
    }

    /**
     * preenche os campos email e senha caso o usuário já tenha digitado e voltou
     * para outros fragments
     */
    private void setPrompts(){
        this.etEmail.setText(this.user.getEmail());
        this.etPassword.setText(this.user.getPassword());
    }



}

