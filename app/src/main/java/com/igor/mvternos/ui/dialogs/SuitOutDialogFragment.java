package com.igor.mvternos.ui.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.igor.mvternos.R;

/**
 * Esta classe representa uma janela que é apresentada quando o usuário tenta colocar
 * um terno esgotado no carrinho
 * @autor Igor Guilherme Almeida Rocha
 */
public class SuitOutDialogFragment extends DialogFragment {

    private Button btBack;
    private Button btChooseSuit;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.dialog_suit_out, container);
        initComponents(view);
        setBtEvents();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Inicializa os componentes
     * @param view Layout inflado
     */
    private void initComponents(View view){
        this.btBack = view.findViewById(R.id.bt_back);
        this.btChooseSuit = view.findViewById(R.id.bt_choose_suit);

    }

    /**
     * Seta os eventos
     */
    private void setBtEvents(){
        this.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dismiss(); }
        });

        this.btChooseSuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { getActivity().finish(); }
        });
    }
}
