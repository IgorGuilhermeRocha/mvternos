package com.igor.mvternos.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.igor.mvternos.R;
import com.igor.mvternos.activity.LoginActivity;
import com.igor.mvternos.controller.RegisterFragment1Controller;
import com.igor.mvternos.domain.model.User;
import com.igor.mvternos.domain.util.Util;
import com.igor.mvternos.domain.util.ValidationsMaskUtil;

/**
 * Classe que representa a primeira parte da activity de registro
 * @author Igor Guilherme Almeida Rocha
 */
public class RegisterFragment1 extends Fragment {

    private RegisterFragment1Controller controller;

    private User user;

    private EditText etName;
    private EditText etCpf;
    private EditText etPhone;

    private Button btNext;
    private Button btBack;

    private RegisterFragment2 registerFragment2;


    public RegisterFragment1(){
    }

    public RegisterFragment1(User user){
        this.user = user;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_register_1, container, false);
        initController(view);
        initComponents(view);
        setEvents();
        setPrompts();
        return view;
    }

    /**
     * Inicializa o controlador
     * @param view View inflada pelo fragment
     */
    private void initController(View view){
        if(this.controller == null){
            this.controller = new RegisterFragment1Controller(getContext(), view);
        }
    }

    /**
     * Inicializa os componentes
     * @param view View inflada pelo fragment
     */
    private void initComponents(View view){
        this.btNext = view.findViewById(R.id.bt_next);
        this.btBack = view.findViewById(R.id.bt_back);
        this.etName = view.findViewById(R.id.et_name);
        this.etCpf = view.findViewById(R.id.et_cpf);
        this.etPhone = view.findViewById(R.id.et_phone);
    }

    /**
     * aloca os eventos
     */
    private void setEvents(){
        setBtEvents();
        setEtEvents();
    }

    /**
     * Adiciona os eventos dos botoes
     */
    private void setBtEvents(){
        this.btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { validForm(); }
        });

        this.btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { backToLogin(); }
        });
    }

    /**
     * Valida o formulário: nome, cpf, telefone
     */
    private void validForm(){
        if(this.controller.validForm(this.etName.getText().toString(), this.etCpf.getText().toString(), this.etPhone.getText().toString())){
            buildFragment();
            Util.changeFragments(registerFragment2, getActivity(), R.id.register_content);
        }
    }

    /**
     * Controi o próximo fragment
     */
    private void buildFragment(){
        buildUser();
        registerFragment2 = new RegisterFragment2(this.user);
    }

    /**
     * Inicializa / modifica o usuário
     */
    private void buildUser(){
        if(this.user == null){
            this.user = new User();
        }
        this.user.setName(etName.getText().toString());
        this.user.setCpf(etCpf.getText().toString());
        this.user.setPhone(etPhone.getText().toString());
    }

    /**
     * Redireciona para a tela de login
     */
    private void backToLogin(){
        startActivity(new Intent(getActivity().getApplicationContext(), LoginActivity.class));
        getActivity().finish();
    }

    /**
     * Adiciona os eventos das caixas de texto
     */
    private void setEtEvents(){
        this.etCpf.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(s.length() >= 1){
                            ValidationsMaskUtil.verifyCpfMask(s, s.length(), s.charAt(s.length() - 1)+"");
                        }

                    }
                }
        );

        this.etPhone.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(s.length() >= 1){
                            ValidationsMaskUtil.verifyPhoneMask(s, s.toString().length(), s.charAt(s.length() - 1)+"");
                        }
                    }
                }
        );
    }

    /**
     * Preenche as caixas de texto com os valores do usuário
     */
    private void setPrompts(){
        if(this.user != null){
            this.etName.setText(user.getName());
            this.etCpf.setText(user.getCpf());
            this.etPhone.setText(user.getPhone());
        }
    }






}
