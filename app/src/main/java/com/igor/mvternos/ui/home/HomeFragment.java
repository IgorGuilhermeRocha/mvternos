package com.igor.mvternos.ui.home;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.igor.mvternos.R;

/**
 * Representa a tela principal, a primeira tela da main activity
 * @author Igor Guilherme Almeida Rocha
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_home, container, false);
        configTela();
        return view;
    }

    /**
     * Se o celular estiver em landscape, muda para portrait, isto é feito para evitar um bug ao sair do vídeo
     * pelo botão back
     */
    private void configTela(){
        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            getActivity().setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

}