package com.igor.mvternos.ui.adjustments;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.igor.mvternos.R;
import com.igor.mvternos.domain.model.interfaces.SendMessageWhatsApp;


/**
 * Classe que representa a tela "ajustes"
 * @autor Igor Guilherme Almeida Rocha
 */
public class AdjustmentsFragment extends Fragment {

    private static SendMessageWhatsApp messageProvider;

    private CardView cardView1;
    private CardView cardView2;
    private CardView cardView3;
    private CardView cardView4;
    private CardView cardView5;
    private CardView cardView6;

    private final String MESSAGE_PROMPT = "Olá, gostaria de um ajuste ";
    private final String CV1_TEXT = "da manga do terno.";
    private final String CV2_TEXT = "do gavião da calça.";
    private final String CV3_TEXT = "das pernas e coxa.";
    private final String CV4_TEXT = "da bainha da calça.";
    private final String CV5_TEXT = "das costas do terno.";
    private final String CV6_TEXT = "do cós da calça.";



    public AdjustmentsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_adjustments, container, false);
        initComponents(view);
        setEvents();
        return view;
    }

    /**
     * Injeção de dependência
     * @param provider Interface para enviar mensagens do whats app
     */
    public static void setProvider(SendMessageWhatsApp provider){
        messageProvider = provider;
    }

    /**
     * Inicializa os componenets
     * @param view view inflada
     */
    private void initComponents(View view){
        this.cardView1 = view.findViewById(R.id.cv_1);
        this.cardView2 = view.findViewById(R.id.cv_2);
        this.cardView3 = view.findViewById(R.id.cv_3);
        this.cardView4 = view.findViewById(R.id.cv_4);
        this.cardView5 = view.findViewById(R.id.cv_5);
        this.cardView6 = view.findViewById(R.id.cv_6);
    }

    /**
     * seta os eventos
     */
    private void setEvents(){
        this.cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { messageProvider.sendMessage(MESSAGE_PROMPT + CV1_TEXT); }
        });

        this.cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { messageProvider.sendMessage(MESSAGE_PROMPT + CV2_TEXT); }
        });

        this.cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { messageProvider.sendMessage(MESSAGE_PROMPT + CV3_TEXT); }
        });

        this.cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { messageProvider.sendMessage(MESSAGE_PROMPT + CV4_TEXT); }
        });

        this.cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { messageProvider.sendMessage(MESSAGE_PROMPT + CV5_TEXT); }
        });

        this.cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { messageProvider.sendMessage(MESSAGE_PROMPT + CV6_TEXT); }
        });
    }



}