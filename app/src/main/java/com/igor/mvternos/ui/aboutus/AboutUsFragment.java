package com.igor.mvternos.ui.aboutus;

import android.content.pm.ActivityInfo;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragmentX;
import com.igor.mvternos.R;


/**
 * Classe que representa a tela "sobre nós"
 * @autor Igor Guilherme Almeida Rocha
 */
public class AboutUsFragment extends Fragment implements YouTubePlayer.OnInitializedListener {

    private final String VIDEO_CODE = "C9nVMQKhfNo";
    private final String YOUTUBE_API_KEY = "AIzaSyC6TdTWOmi1N11YYP8nP2Bq0q6lQS8j9Y4";
    private YouTubePlayer.OnFullscreenListener fullscreenListener;


    public AboutUsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        YouTubePlayerSupportFragmentX youTubePlayerFragment = YouTubePlayerSupportFragmentX.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.youtube_fragment, youTubePlayerFragment).commit();
        setListeners();

        youTubePlayerFragment.initialize(YOUTUBE_API_KEY, this);
        return view;
    }

    /**
     * Seta os listeners
     */
    private void setListeners(){
        fullscreenListener = new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean isFullscreen) {
                if(!isFullscreen){
                    getActivity().setRequestedOrientation(
                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        };
    }


    /**
     * Método que é chamado quando o player do youtube é incializado com sucesso
     * @param provider representa o provedor do vídeo
     * @param youTubePlayer Objeto que representa o player do youtube
     * @param wasRestored boolean, se true quer dizer que o vídeo foi restaurado
     */
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setOnFullscreenListener(this.fullscreenListener);
        youTubePlayer.cueVideo(VIDEO_CODE);
    }


    /**
     * Método chamado quando ocorre um erro ao inicializar o vídeo
     * @param provider provedor do vídeo
     * @param youTubeInitializationResult resultado, erro
     */
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

}