package com.igor.mvternos.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.igor.mvternos.R;
import com.igor.mvternos.domain.util.Util;
import com.igor.mvternos.domain.util.ValidationMessages;
import com.igor.mvternos.domain.util.ValidationsMaskUtil;

/**
 * Classe Controladora da RegisterFragment1
 * @autor Igor Guilherme Almeida Rocha
 * @see com.igor.mvternos.ui.register.RegisterFragment1
 */
public class RegisterFragment1Controller {

    private LayoutInflater inflater;
    private Context context;
    private View layout;


    public RegisterFragment1Controller(Context context, View layout){
        this.context = context;
        this.layout = layout;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Método reponsável por validar o preenchimento dos campos passados como parametro
     * @param nome Nome do usuário
     * @param cpf Cpf do usuário
     * @param phone Telefone do usuário
     * @return true se todos os campos foram preenchidos
     */
    public Boolean validForm(String nome, String cpf, String phone){
        boolean isBlank = Util.validFields(new String[]{nome, cpf, phone});
        if(!isBlank) {
            showToast(2, ValidationMessages.FIELDS_BLANK_WARNING);
            return false;
        }else return validCpfAndPhone(cpf, phone);

    }

    /**
     * Valida os campos cpf e telefone
     * @param cpf Cpf do usuário
     * @param phone Telefone do usuário
     * @return true se ambos os campos estiverem preenchidos corretamente
     */
    private Boolean validCpfAndPhone(String cpf, String phone){
        if(!ValidationsMaskUtil.validCpf(cpf)){
            showToast(1, ValidationMessages.CPF_PATTERN_ERROR);
            return false;
        }else if(!ValidationsMaskUtil.validPhone(phone)){
            showToast(1, ValidationMessages.PHONE_PATTERN_ERROR);
            return false;
        }
        return true;
    }

    /**
     * Mostra uma mensagem customizada para o usuário
     * @param message Mensagem a ser mostrada pelo Toast
     * @param type indica o tipo do toast 1 para erro 2 para aviso
     */
    private void showToast ( int type, String message){
        ViewGroup toastContainer = this.layout.findViewById(R.id.container_toast);
        View toastStyle = this.inflater.inflate(R.layout.custom_toast, toastContainer);
        Util.showCustomizedToast(this.context, type, message, toastStyle);
    }
}
