package com.igor.mvternos.controller;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.igor.mvternos.R;
import com.igor.mvternos.activity.SuitDetailsActivity;
import com.igor.mvternos.adapters.SuitAdapter;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.domain.model.Suit;
import com.igor.mvternos.domain.model.enums.SuitSize;
import com.igor.mvternos.domain.model.interfaces.RecyclerSuitOnClick;
import com.igor.mvternos.domain.model.interfaces.UpdateItemCount;
import com.igor.mvternos.provider.OrderProvider;
import com.igor.mvternos.repository.SuitRepository;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Controladora da SuitFragment
 * @autor Igor Guilherme Almeida Rocha
 * @see com.igor.mvternos.ui.suits.SuitsFragment
 */
public class SuitsFragmentController implements RecyclerSuitOnClick, UpdateItemCount {

    private TextView tvItemCount;
    private LinearLayout pbLayout;
    private ProgressBar pb;
    private SuitAdapter adapter;
    private LinearLayout layoutSearchBar;
    private LayoutInflater inflater;
    private Context context;
    private View layout;
    private StorageReference storageReference = FirebaseConfig.getFirebaseStorageReference();
    private final long ONE_MEGABYTE = 1024 * 1024;
    private static List<Suit> suits;




    // Listener que escuta todas as modificações da lista de ternos no banco
    private ValueEventListener listenerSuitList = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot snapshot) {
            recoverSuits(snapshot);

        }
        @Override
        public void onCancelled(@NonNull DatabaseError error) {}
    };


    public SuitsFragmentController(Context context, View layout){
        OrderProvider.setUpdateItemCount(this);
        OrderProvider.getCountOfOrders();
        this.context = context;
        this.layout = layout;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.pbLayout = layout.findViewById(R.id.pb_layout);
        this.pb = layout.findViewById(R.id.loading_suits);
        this.layoutSearchBar = layout.findViewById(R.id.layout_search_bar);
        this.tvItemCount = this.layoutSearchBar.findViewById(R.id.tv_item_count);

    }

    // atualiza os ternos
    public void updateList(){
        SuitRepository.getAllSuits(listenerSuitList);
    }

    /**
     * Recupera as ternos no banco
     * @param snapshot Objeto que faz acesso aos dados recuperados
     * @return Uma lista de suit
     */
    private void recoverSuits(DataSnapshot snapshot){
        long maxItens = snapshot.getChildrenCount();
        suits = new ArrayList<>();
        for(DataSnapshot ds : snapshot.getChildren()){
            Suit suit = new Suit();
            suit.setId(ds.child("id").getValue(String.class));
            suit.setName(ds.child("name").getValue(String.class));
            suit.setValue(Double.valueOf(ds.child("price").getValue(Double.class)));
            suit.setDescription(ds.child("description").getValue(String.class));
            StorageReference imgref = storageReference.child("images/suit_imgs").child(suit.getId() + ".png");
            imgref.getBytes(ONE_MEGABYTE).addOnCompleteListener(
                    new OnCompleteListener<byte[]>() {
                        @Override
                        public void onComplete(@NonNull Task<byte[]> task) {
                            if(task.isSuccessful()) {
                                suit.setImgBitmap((BitmapFactory.decodeByteArray(task.getResult(), 0, task.getResult().length)));
                            }
                            suits.add(suit);
                            if(suits.size() == maxItens){
                                stopLoadingScreen(suits);
                            }
                        }
                    }
            );
        }

    }


    /**
     * Método que para a tela de loading e mostra a lista de ternos
     * @param suits Lista de ternos
     */
    private void stopLoadingScreen(List<Suit> suits){
        this.layoutSearchBar.setVisibility(View.VISIBLE);
        this.pbLayout.setVisibility(View.GONE);
        this.pb.setVisibility(View.GONE);
        configSuitList(layout, suits);

    }



    /**
     * Método que confira a recycler view
     * @param view View atual
     * @param listSuit lista de ternos
     */
    private void configSuitList(View view, List<Suit> listSuit){
        this.adapter = new SuitAdapter(listSuit, this.context, this);
        RecyclerView rvSuits = view.findViewById(R.id.rv_suits);
        rvSuits.setHasFixedSize(true);
        rvSuits.setLayoutManager(new LinearLayoutManager(this.context));
        rvSuits.setAdapter(this.adapter);
    }

    /**
     * Método que faz o filtro de ternos com base no texto recebido como parametro
     * @param text Texto digitado pelo usuário
     */
    public void search(String text){
        if(this.adapter != null){
            this.adapter.getFilter().filter(text);
        }
    }


    /**
     * Método chamado após um item da lista ser clicado, este método abre uma outra
     * activity com os dados do Terno
     * @param suit Terno que foi clicado, Instãncia de suit
     */
    @Override
    public void onItemClicked(Suit suit) {
        Intent intent = new Intent(context, SuitDetailsActivity.class);
        intent.putExtra("suit", suit);
        context.startActivity(intent);
    }

    /**
     * Retorna uma cópia da lista de ternos
     * @return
     */
    public static List<Suit> getSuitList(){
        return List.copyOf(suits);
    }

    /**
     * Método que faz a atualização da quantidade de itens no carrinho
     * @param count número de itens no carrinho
     */
    @Override
    public void updateCount(long count) {
        this.tvItemCount.setText(count+"");
    }
}
