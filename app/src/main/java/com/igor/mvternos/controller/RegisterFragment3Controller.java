package com.igor.mvternos.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.igor.mvternos.R;
import com.igor.mvternos.activity.LoginActivity;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.domain.model.User;
import com.igor.mvternos.domain.util.Util;
import com.igor.mvternos.domain.util.ValidationMessages;
import com.igor.mvternos.domain.util.ValidationsMaskUtil;

/**
 * Classe Controladora da RegisterFragment3
 * @autor Igor Guilherme Almeida Rocha
 * @see com.igor.mvternos.ui.register.RegisterFragment3
 */
public class RegisterFragment3Controller {

    private FirebaseAuth auth = FirebaseConfig.getFirebaseAuth();
    private DatabaseReference database = FirebaseConfig.getDatabaseReference();
    private StorageReference storage = FirebaseConfig.getFirebaseStorageReference();

    private LayoutInflater inflater;
    private Context context;
    private View layout;

    public RegisterFragment3Controller(Context context, View layout){
        this.context = context;
        this.layout = layout;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    /**
     * Faz o registro do usuário no banco
     * @param user Instância de User a ser adicionada
     */
    public void registerUser(User user){
        this.auth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword()
        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) onSuccessfulRegister(task, user);
                        else onFailureRegister(task);
                    }
                }
        );
    }

    /**
     * Método que faz as operações necessárias, quando o registro for um sucesso
     * @param task Objeto que representa o retorno de uma operação do firebase
     * @param user Usuário a ser adicionado
     */
    private void onSuccessfulRegister(Task<AuthResult> task, User user){
        String id = task.getResult().getUser().getUid();
        user.setId(id);
        saveImg(user);
        database.child("user").child(id).setValue(user);
        toLogin();
    }

    /**
     * Método que salva a imagem do usuário no banco
     * @param user Instância de user
     */
    private void saveImg(User user){
        if(user.getImgBitmap() != null){
            byte [] imgBytes = Util.bitmapToByte(user.getImgBitmap(), 75, Bitmap.CompressFormat.JPEG);
            StorageReference imgRef = storage.child("images/user_imgs").child(user.getId()+".jpeg");
            imgRef.putBytes(imgBytes);
        }

    }

    /**
     * Redireciona para a tela de login
     */
    private void toLogin(){
        context.startActivity(new Intent(context, LoginActivity.class));
        ((Activity) context).finish();
    }

    /**
     * Método que mostra um toast relacionado com o tipo de erro
     * que ocorreu ao tentar registrar o usuário
     * @param task Objeto que representa o retorno de uma operação do firebase
     */
    private void onFailureRegister(Task<AuthResult> task){
        try {
            throw task.getException();
        }catch (FirebaseAuthUserCollisionException e) {
            showToast(1, ValidationMessages.EMAIL_ALREADY_USE_ERROR);
        }catch (FirebaseAuthInvalidCredentialsException e){
            showToast(1, ValidationMessages.EMAIL_BADLY_FORMATTED_ERROR);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Valida o formulário preenchido pelo usuário
     * @param email Email do usuário
     * @param password Senha do usuário
     * @param confirmPassword Campo de confirmação de senha
     * @return true, se o formulário foi preenchido corretamente
     */
    public Boolean validForm(String email, String password, String confirmPassword){
        if(Util.validFields(new String[]{email, password, confirmPassword})){
            if(ValidationsMaskUtil.validPassword(password)){
                return verifyPasswords(password, confirmPassword);
            }else{
                showToast(2, ValidationMessages.PASSWORD_LENGTH_WARNING);
                return false;
            }
        } else {
            showToast(2,ValidationMessages.FIELDS_BLANK_WARNING);
            return false;
        }

    }

    /**
     * Verifica se o usuário digitou a mesma senha em ambos os campos
     * @param password Senha do usuário
     * @param confirmPassword Campo de confirmação de senha
     * @return true se ele digitou a mesma senha em ambos os campos
     */
    private Boolean verifyPasswords(String password, String confirmPassword){
        if(!password.equals(confirmPassword)){
            showToast(2,ValidationMessages.PASSWORD_MATCHES_WARNING);
            return false;
        }
        return true;
    }


    /**
     * Mostra uma mensagem customizada para o usuário
     * @param message Mensagem a ser mostrada pelo Toast
     * @param type indica o tipo do toast 1 para erro 2 para aviso
     */
    private void showToast ( int type, String message){
        ViewGroup toastContainer = this.layout.findViewById(R.id.container_toast);
        View toastStyle = this.inflater.inflate(R.layout.custom_toast, toastContainer);
        Util.showCustomizedToast(this.context, type, message, toastStyle);
    }
}
