package com.igor.mvternos.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.igor.mvternos.R;
import com.igor.mvternos.activity.MainActivity;
import com.igor.mvternos.activity.RegisterActivity;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.config.SharedPreferencesConfig;
import com.igor.mvternos.domain.util.DecodeAndEncodeUtil;
import com.igor.mvternos.domain.util.Util;
import com.igor.mvternos.domain.util.ValidationMessages;

import java.util.HashMap;

/**
 * Classe controladora da activity de login
 * @see com.igor.mvternos.activity.LoginActivity
 * @author Igor Guilherme Almeida Rocha
 */
public class LoginActivityController {

    private FirebaseAuth firebaseAuth = FirebaseConfig.getFirebaseAuth();
    private LayoutInflater inflater;
    private Context context;
    private View layout;

    public LoginActivityController(Context context, View layout) {
        this.context = context;
        this.layout = layout;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     *
     * @param email email digitado pelo usuário
     * @param password senha digitada pelo usuário
     * @param isChecked true se o usuário quer que suas credenciais sejam lembradas
     * @return true se ocorreu tudo bem ao fazer o login
     */
    public void login(String email, String password, Boolean isChecked) {
        Boolean isValid = Util.validFields(new String[]{email, password});
        if (!isValid) showToast(2, ValidationMessages.FIELDS_BLANK_WARNING);
        else {
            this.firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) onFailureLogin(task);
                            else onSuccessfulLogin(email, password, isChecked);
                        }
                    }
            );
        }
    }

    /**
     * Este método é chamado quando a requisição falha
     * Método responsável por tratar os erros de login
     * @param task tarefa assíncrona
     *
     */
    private void onFailureLogin(Task<AuthResult> task){
        try {
            throw task.getException();
        } catch (FirebaseAuthInvalidUserException e) {
            showToast(1, "Email inválido");
        } catch (FirebaseAuthInvalidCredentialsException e) {
            showToast(1, "Email ou Senha incorreto(s)");
        } catch (Exception e) {
            e.printStackTrace();
            showToast(1, "Erro ao entrar");
        }
    }

    /**
     * Este método é chamado quando a requisição deu certo
     * Método que redireciona para a tela principal do app
     * @param email Email do usuário
     * @param password Senha do usuário
     * @param isChecked boolean que identifica se o usuario quer que suas credenciais sejam salvas
     */
    private void onSuccessfulLogin(String email, String password, Boolean isChecked){
        modifyCredentials(email, password, isChecked);
        context.startActivity(new Intent(context, MainActivity.class));
        ((Activity) context).finish();
    }


    /**
     * Método que salva as credenciais do usuário, no arquivo de preferências
     * @see DecodeAndEncodeUtil
     * @see SharedPreferencesConfig
     * @param email Email do usuário
     * @param password Senha do usuário
     * @param isChecked opção de salvar ou não as credenciais
     */
    private void modifyCredentials(String email, String password, Boolean isChecked){
        SharedPreferences.Editor sp = SharedPreferencesConfig.getSharedEditor(context,
                SharedPreferencesConfig.USER_CREDENTIALS, SharedPreferencesConfig.PRIVATE);
        if(!isChecked) {
            email = password = "";
            isChecked = false;
        }

        sp.putString("email", DecodeAndEncodeUtil.encodeToBase64(email));
        sp.putString("password", DecodeAndEncodeUtil.encodeToBase64(password));
        sp.putString("isChecked", isChecked+"");
        sp.commit();
    }

    /**
     * Método que redireciona o usuário para a tela de cadastro
     */
    public void register(){
        this.context.startActivity(new Intent(context, RegisterActivity.class));
        ((Activity) context).finish();

    }


    /**
     * Método que pega as credenciais salvas
     * @see DecodeAndEncodeUtil
     * @see SharedPreferencesConfig
     * @return hashmap<String, String> com as credenciais do usuário
     */
    public HashMap<String, String> getCredentials(){
        HashMap<String, String> credentials = new HashMap<>();
        SharedPreferences sp = SharedPreferencesConfig.getSharedRead(context,
                SharedPreferencesConfig.USER_CREDENTIALS, SharedPreferencesConfig.PRIVATE);
        credentials.put("email", DecodeAndEncodeUtil.decodeBase64(sp.getString("email", "")));
        credentials.put("password", DecodeAndEncodeUtil.decodeBase64(sp.getString("password", "")));
        credentials.put("isChecked", sp.getString("isChecked", "false"));
        return credentials;
    }

    /**
     * Mostra uma mensagem customizada para o usuário
     * @param message Mensagem a ser mostrada pelo Toast
     * @param type indica o tipo do toast 1 para erro 2 para aviso
     */
    private void showToast ( int type, String message){
        ViewGroup toastContainer = this.layout.findViewById(R.id.container_toast);
        View toastStyle = this.inflater.inflate(R.layout.custom_toast, toastContainer);
        Util.showCustomizedToast(this.context, type, message, toastStyle);
    }

}
