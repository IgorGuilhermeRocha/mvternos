package com.igor.mvternos.controller;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.igor.mvternos.config.FirebaseConfig;
import com.igor.mvternos.domain.model.Order;
import com.igor.mvternos.domain.model.Suit;
import com.igor.mvternos.domain.model.enums.SuitSize;
import com.igor.mvternos.provider.SuitStockProvider;

public class SuitDetailsActivityController {

    private DatabaseReference databaseReference;

    public SuitDetailsActivityController(){
        databaseReference = FirebaseConfig.getDatabaseReference();

    }

    public void addOrderToCart(Suit suit, Long quantity, SuitSize size){
        double orderPrice = calcPrice(suit, quantity);
        Order order = new Order(suit, quantity, size, orderPrice);
        FirebaseUser user = FirebaseConfig.getFirebaseAuth().getCurrentUser();
        DatabaseReference aux = databaseReference.child("user/" +  user.getUid() + "/shopping_cart");
        DatabaseReference aux2 = aux.push();
        String orderID = aux2.getKey();
        order.setId(orderID);
        aux2.setValue(order);
        SuitStockProvider.removeStock(order);
    }

    private Double calcPrice(Suit suit, Long quantity){
        return suit.getValue() * quantity;
    }
}
