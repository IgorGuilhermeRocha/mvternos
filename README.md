# MV ternos
Este projeto é um aplicativo android feito para uma loja de ternos real, de Conselheiro Lafaiete. <br>

O projeto conta com as seguintes funcionalidades:

<ul>
    <li>Tela de login</li>
    <li>Telas de Cadastro</li>
    <li>Menu de navegação</li>
    <li>Visualização e compra dos ternos</li>
    <li>Edição de perfil ( em andamento ) </li>
    <li>Localização da loja física ( não implementado ainda ) </li>
</ul>

Stacks: 
<ul>
    <li>Java</li>
    <li>XML</li>
    <li>Firebase</li>
</ul>

## Detalhes do projeto
Abaixo irei ressaltar todas as funcionalidades já citadas.

### Tela de login

<img src="imgs_readme/login_screen.jpeg" width="350" height="800">

### Telas de cadastro

<img src="imgs_readme/cad_1.jpeg" width="350" height="800">
<img src="imgs_readme/cad_2.jpeg" width="350" height="800">
<img src="imgs_readme/cad_3.jpeg" width="350" height="800">

### Menu de navegação
<img src="imgs_readme/menu.jpeg" width="350" height="800">

### Visualização e compra dos ternos
<img src="imgs_readme/suits.jpeg" width="350" height="800">

### Tela de detalhes do terno
<img src="imgs_readme/details.jpeg" width="350" height="800">

<br><br>
Acho que a parte essencial do projeto foi mostrada, claro ainda existem várias coisas não citadas, como cada um dos itens de menu, porém 
se você quiser testar profundamente o app vá em app > relase, lá deve estar localizado o apk do app, como o app ainda esta sendo desenvolvido não garanto que esteja atualizado.


